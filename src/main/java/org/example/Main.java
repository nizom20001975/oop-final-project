package org.example;

import org.example.controller.BrandController;
import org.example.controller.CategoryController;
import org.example.controller.ItemController;
import org.example.entity.BrandEntity;
import org.example.entity.CategoryEntity;
import org.example.entity.ItemEntity;
import org.example.service.BrandService;
import org.example.service.CategoryService;
import org.example.service.ItemService;

import java.util.List;
import java.util.Scanner;

public class Main {
    private static Long id = 1L;

    public static void main(String[] args) {
        ItemController itemController = new ItemController();
        BrandController brandController = new BrandController();
        CategoryController categoryController = new CategoryController();

        boolean main = true;
        System.out.println("Project");

        Scanner scanner = new Scanner(System.in);
        while (main) {

            System.out.println("""
                    1. BRAND
                    2. CATEGORY
                    3. ITEM
                    0. EXIT
                    """);
            while (!scanner.hasNextInt()) {
                System.out.println("Please enter number");
                scanner.next();
            }
            int menu = scanner.nextInt();

            if (menu == 1) {
                boolean brandRunning = true;
                while (brandRunning) {
                    scanner = new Scanner(System.in);

                    System.out.println("""
                            1. ADD BRAND
                            2. EDIT BRAND
                            3. GET ALL BRAND
                            4. REMOVE BRAND
                            0. EXIT
                            """);
                    while (!scanner.hasNextInt()) {
                        System.out.println("Please enter number");
                        scanner.next();
                    }
                    int command = scanner.nextInt();

                    if (command == 1) {
                        scanner = new Scanner(System.in);

                        System.out.println("Enter name item");
                        String name = scanner.nextLine();

                        brandController.add(new BrandEntity(
                                id++,
                                name
                        ));
                        System.out.println("Successfully added");
                    }

                    if (command == 2) {
                        List<ItemEntity> brands = itemController.getAll();
                        brands.forEach(System.out::println);
                        System.out.println("Enter id editing item");

                        scanner = new Scanner(System.in);
                        Long id = scanner.nextLong();

                        scanner = new Scanner(System.in);

                        System.out.println("Enter new name item");
                        String name = scanner.nextLine();

                        brandController.edit(
                                id,
                                new BrandEntity(
                                        id,
                                        name
                                ));
                        System.out.println("Successfully edited");
                    }

                    if (command == 3) {
                        List<BrandEntity> brands = brandController.getAll();
                        brands.forEach(BrandService::getFormatEntity);
                    }
                    if (command == 4) {
                        scanner = new Scanner(System.in);
                        System.out.println("Please enter you deleting item Id");
                        Long id = scanner.nextLong();

                        brandController.remove(id);
                        System.out.println("Successfully deleted");
                    }

                    if (command == 0) {
                        brandRunning = false;
                    }

                }
            }

            if (menu == 2) {
                boolean categoryRunning = true;
                while (categoryRunning) {

                    scanner = new Scanner(System.in);

                    System.out.println("""
                            1. ADD CATEGORY
                            2. EDIT CATEGORY
                            3. GET ALL CATEGORY
                            4. REMOVE CATEGORY
                            0. EXIT
                            """);
                    while (!scanner.hasNextInt()) {
                        System.out.println("Please enter number");
                        scanner.next();
                    }
                    int command = scanner.nextInt();

                    if (command == 1) {
                        scanner = new Scanner(System.in);

                        System.out.println("Enter name category");
                        String name = scanner.nextLine();

                        categoryController.add(new CategoryEntity(
                                id++,
                                name
                        ));
                        System.out.println("Successfully added");
                    }

                    if (command == 2) {
                        List<CategoryEntity> categoryEntities = categoryController.getAll();
                        categoryEntities.forEach(System.out::println);
                        System.out.println("Enter id editing category");

                        scanner = new Scanner(System.in);
                        Long id = scanner.nextLong();

                        scanner = new Scanner(System.in);

                        System.out.println("Enter new name category");
                        String name = scanner.nextLine();

                        categoryController.edit(
                                id,
                                new CategoryEntity(
                                        id,
                                        name
                                ));
                        System.out.println("Successfully edited");
                    }

                    if (command == 3) {
                        List<CategoryEntity> categoryEntities = categoryController.getAll();
                        categoryEntities.forEach(CategoryService::getFormatEntity);
                    }
                    if (command == 4) {
                        scanner = new Scanner(System.in);
                        System.out.println("Please enter you deleting category Id");
                        Long id = scanner.nextLong();

                        categoryController.remove(id);
                        System.out.println("Successfully deleted");
                    }

                    if (command == 0) {
                        categoryRunning = false;
                    }
                }
            }

            if (menu == 3) {
                boolean itemRunning = true;
                while (itemRunning) {
                    try {
                        scanner = new Scanner(System.in);

                        System.out.println("""
                                1. ADD ITEM
                                2. EDIT ITEM
                                3. GET ALL ITEM
                                4. REMOVE ITEM
                                5. SEARCH
                                0. EXIT
                                """);
                        int command = scanner.nextInt();

                        if (command == 1) {
                            scanner = new Scanner(System.in);

                            System.out.println("Enter name item");
                            String name = scanner.nextLine();

                            System.out.println("Enter price item");

                            scanner = new Scanner(System.in);

                            while (!scanner.hasNextDouble()) {
                                System.out.println("Please write in number form");
                                scanner.next();
                            }
                            Double price = scanner.nextDouble();

                            scanner = new Scanner(System.in);

                            System.out.println("Enter color item");
                            String color = scanner.nextLine();

                            System.out.println("Enter registrationDate item");
                            String registrationDate = scanner.nextLine();

                            List<BrandEntity> brands = brandController.getAll();
                            brands.forEach(BrandService::getFormatEntity);

                            System.out.println("Enter brand id");
                            String brand = scanner.nextLine();

                            List<CategoryEntity> categoryEntities = categoryController.getAll();
                            categoryEntities.forEach(CategoryService::getFormatEntity);

                            System.out.println("Enter category Id item");
                            String category = scanner.nextLine();

                            itemController.add(new ItemEntity(
                                    id++,
                                    name,
                                    price,
                                    color,
                                    registrationDate,
                                    Long.valueOf(brand),
                                    Long.valueOf(category)
                            ));
                            System.out.println("Successfully added");
                        }

                        if (command == 2) {
                            List<ItemEntity> brands = itemController.getAll();
                            brands.forEach(System.out::println);
                            System.out.println("Enter id editing item");

                            scanner = new Scanner(System.in);
                            Long id = scanner.nextLong();

                            scanner = new Scanner(System.in);

                            System.out.println("Enter new name item");
                            String name = scanner.nextLine();

                            System.out.println("Enter new price item");
                            Double price = scanner.nextDouble();

                            scanner = new Scanner(System.in);

                            System.out.println("Enter new color item");
                            String color = scanner.nextLine();

                            System.out.println("Enter new registrationDate item");
                            String registrationDate = scanner.nextLine();


                            List<BrandEntity> brandEntities = brandController.getAll();
                            brandEntities.forEach(BrandService::getFormatEntity);


                            System.out.println("Enter new brand item");
                            String brand = scanner.nextLine();

                            List<CategoryEntity> categoryEntities = categoryController.getAll();
                            categoryEntities.forEach(CategoryService::getFormatEntity);


                            System.out.println("Enter new category item");
                            String category = scanner.nextLine();

                            itemController.edit(
                                    id,
                                    new ItemEntity(
                                            id,
                                            name,
                                            price,
                                            color,
                                            registrationDate,
                                            Long.valueOf(brand),
                                            Long.valueOf(category)
                                    ));
                            System.out.println("Successfully edited");
                        }

                        if (command == 3) {
                            List<ItemEntity> items = itemController.getAll();
                            items.forEach(ItemService::getFormatEntity);
                        }
                        if (command == 4) {
                            scanner = new Scanner(System.in);
                            List<ItemEntity> items = itemController.getAll();
                            items.forEach(ItemService::getFormatEntity);
                            System.out.println("Please enter you deleting item Id");
                            Long id = scanner.nextLong();

                            itemController.remove(id);
                            System.out.println("Successfully deleted");
                        }
                        if (command == 5) {

                            System.out.println("Enter searching text");

                            scanner = new Scanner(System.in);
                            String text = scanner.nextLine();

                            List<ItemEntity> search = itemController.search(text);
                            search.forEach(System.out::println);


                        }
                        if (command == 0) {
                            itemRunning = false;
                        }
                    } catch (Exception e) {
                        System.err.println("ERROR");
                    }
                }
            }


            if (menu == 0) {
                main = false;
            }
        }

    }
}


