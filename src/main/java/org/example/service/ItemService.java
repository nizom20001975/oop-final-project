package org.example.service;

import org.example.controller.BrandController;
import org.example.controller.CategoryController;
import org.example.data.DataItem;
import org.example.entity.BrandEntity;
import org.example.entity.CategoryEntity;
import org.example.entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ItemService {

    private final DataItem dataItem;

    public ItemService() {
        dataItem = new DataItem();
    }


    public List<ItemEntity> add(ItemEntity item) {

        List<ItemEntity> items = dataItem.getItem();

        ArrayList<ItemEntity> itemEntities = new ArrayList<>(items);
        itemEntities.add(item);

        dataItem.writeBrandData(itemEntities);

        return dataItem.getItem();

    }

    public boolean edit(Long id, ItemEntity editedItem) {

        ItemEntity item = getById(id);

        if (Objects.isNull(item)) {
            return false;
        }

        if (Objects.nonNull(editedItem.getName()) && !"".equals(editedItem.getName())) {
            item.setName(editedItem.getName());

        }
        if (Objects.nonNull(editedItem.getPrice())) {
            item.setPrice(editedItem.getPrice());

        }
        if (Objects.nonNull(editedItem.getColor()) && !"".equals(editedItem.getColor())) {
            item.setColor(editedItem.getColor());

        }
        if (Objects.nonNull(editedItem.getRegistrationDate()) && !"".equals(editedItem.getRegistrationDate())) {
            item.setRegistrationDate(editedItem.getRegistrationDate());

        }
//        if (Objects.nonNull(editedItem.getBrand()) && !"".equals(editedItem.getBrand())) {
//            item.setBrand(editedItem.getBrand());
//
//        }
//        if (Objects.nonNull(editedItem.getCategory()) && !"".equals(editedItem.getCategory())) {
//            item.setCategory(editedItem.getCategory());
//
//        }

        List<ItemEntity> itemEntities = getAll();

        List<ItemEntity> collect = itemEntities
                .stream()
                .filter(i -> !Objects.equals(i.getId(), id))
                .collect(Collectors.toList());
        collect.add(item);

        dataItem.writeBrandData(collect);

        return true;
    }

    public ItemEntity getById(Long id) {
        return dataItem.getItem(id);
    }

    public boolean remove(Long id) {
        List<ItemEntity> brandList = dataItem.getItem();

        ItemEntity removedBrand = getById(id);

        if (Objects.isNull(removedBrand)) {
            return false;
        }

        List<ItemEntity> collect = brandList.stream().filter(item -> !Objects.equals(item.getId(), id)).collect(Collectors.toList());

        dataItem.writeBrandData(collect);

        return true;
    }

    public List<ItemEntity> getAll() {

        return dataItem.getItem();
    }

    public List<ItemEntity> search(String searchingText) {

        List<ItemEntity> items = dataItem.getItem();
        List<ItemEntity> result = new ArrayList<>();

        for (ItemEntity item : items) {
            if (item.getName().contains(searchingText)) {
                result.add(item);
            }
        }
        return result;
    }


    public static void getFormatEntity(ItemEntity item) {
        CategoryController categoryController = new CategoryController();
        BrandController brandController = new BrandController();

        String categoryName = categoryController.getAll().stream().filter(i -> Objects.equals(i.getId(), item.getCategoryId())).findAny().orElse(new CategoryEntity()).getName();
        String brandName = brandController.getAll().stream().filter(i -> Objects.equals(i.getId(), item.getBrandId())).findAny().orElse(new BrandEntity()).getName();

        String result =
                "Id : " + item.getId() + " | " +
                        "Name : " + item.getName() + " | " +
                        "Price : " + item.getPrice() + " | " +
                        "Color : " + item.getColor() + " | " +
                        "Registration Date : " + item.getRegistrationDate() + " | " +
                        "Brand : " + brandName + " | " +
                        "Category : " + categoryName + " | ";
        System.out.println(result);
    }
}





