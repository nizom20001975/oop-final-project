package org.example.service;

import org.example.data.DataBrand;
import org.example.entity.BrandEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BrandService {
    private final DataBrand dataBrand;

    public BrandService() {
        dataBrand = new DataBrand();
    }

    public BrandService(DataBrand dataBrand) {
        this.dataBrand = dataBrand;
    }


    public List<BrandEntity> add(BrandEntity item) {

        List<BrandEntity> items = dataBrand.getBrand();

        ArrayList<BrandEntity> itemEntities = new ArrayList<>(items);
        itemEntities.add(item);

        dataBrand.writeBrandData(itemEntities);

        return dataBrand.getBrand();

    }

    public boolean edit(Long id, BrandEntity editedItem) {

        BrandEntity item = getById(id);

        if (Objects.isNull(item)) {
            return false;
        }

        if (Objects.nonNull(editedItem.getName()) && !"".equals(editedItem.getName())) {
            item.setName(editedItem.getName());

        }

        List<BrandEntity> itemEntities = getAll();

        List<BrandEntity> collect = itemEntities
                .stream()
                .filter(i -> !Objects.equals(i.getId(), id))
                .collect(Collectors.toList());
        collect.add(item);

        dataBrand.writeBrandData(collect);

        return true;
    }

    public BrandEntity getById(Long id) {
        return dataBrand.getBrand(id);
    }

    public boolean remove(Long id) {
        List<BrandEntity> brandList = dataBrand.getBrand();

        BrandEntity removedBrand = getById(id);

        if (Objects.isNull(removedBrand)) {
            return false;
        }

        List<BrandEntity> collect = brandList.stream().filter(item -> !Objects.equals(item.getId(), id)).collect(Collectors.toList());

        dataBrand.writeBrandData(collect);

        return true;
    }

    public List<BrandEntity> getAll() {

        return dataBrand.getBrand();
    }

    public static void getFormatEntity(BrandEntity item) {
        String result =
                "Id : " + item.getId() + " | " +
                        "Name : " + item.getName() + " | ";
        System.out.println(result);
    }
}
