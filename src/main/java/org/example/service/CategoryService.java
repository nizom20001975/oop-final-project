package org.example.service;

import org.example.data.DataCategory;
import org.example.entity.CategoryEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CategoryService {
    private final DataCategory dataCategory;

    public CategoryService() {
        dataCategory = new DataCategory();
    }

    public CategoryService(DataCategory dataCategory) {
        this.dataCategory = dataCategory;
    }


    public List<CategoryEntity> add(CategoryEntity item) {

        List<CategoryEntity> items = dataCategory.getCategory();

        ArrayList<CategoryEntity> itemEntities = new ArrayList<>(items);
        itemEntities.add(item);

        dataCategory.writeCategoryData(itemEntities);

        return dataCategory.getCategory();

    }

    public boolean edit(Long id, CategoryEntity editedItem) {

        CategoryEntity item = getById(id);

        if (Objects.isNull(item)) {
            return false;
        }

        if (Objects.nonNull(editedItem.getName()) && !"".equals(editedItem.getName())) {
            item.setName(editedItem.getName());

        }

        List<CategoryEntity> itemEntities = getAll();

        List<CategoryEntity> collect = itemEntities
                .stream()
                .filter(i -> !Objects.equals(i.getId(), id))
                .collect(Collectors.toList());
        collect.add(item);

        dataCategory.writeCategoryData(collect);

        return true;
    }

    public CategoryEntity getById(Long id) {
        return dataCategory.getCategory(id);
    }

    public boolean remove(Long id) {
        List<CategoryEntity> brandList = dataCategory.getCategory();

        CategoryEntity removedCategory = getById(id);

        if (Objects.isNull(removedCategory)) {
            return false;
        }

        List<CategoryEntity> collect = brandList.stream().filter(item -> !Objects.equals(item.getId(), id)).collect(Collectors.toList());

        dataCategory.writeCategoryData(collect);

        return true;
    }

    public List<CategoryEntity> getAll() {

        return dataCategory.getCategory();
    }

    public static void getFormatEntity(CategoryEntity item) {
        String result =
                "Id : " + item.getId() + " | " +
                        "Name : " + item.getName() + " | ";
        System.out.println(result);
    }
}
