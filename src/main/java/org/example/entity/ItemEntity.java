package org.example.entity;


public class ItemEntity {
    private Long id;

    private String name;

    private Double price;

    private String color;
    private String registrationDate;
    private Long brandId;
    private Long categoryId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public ItemEntity() {
    }

    public ItemEntity(Long id, String name, Double price, String color, String registrationDate, Long brandId, Long categoryId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.color = color;
        this.registrationDate = registrationDate;
        this.brandId = brandId;
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "ItemEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", color='" + color + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                ", brandId=" + brandId +
                ", categoryId=" + categoryId +
                '}';
    }
}
