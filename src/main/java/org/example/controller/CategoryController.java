package org.example.controller;

import org.example.entity.CategoryEntity;
import org.example.service.CategoryService;

import java.util.List;

public class CategoryController {

    private CategoryService categoryService;

    public CategoryController() {
        categoryService = new CategoryService();
    }

    public List<CategoryEntity> add(CategoryEntity category) {
        return categoryService.add(category);
    }

    public boolean edit(Long id, CategoryEntity editedCategory) {
        return categoryService.edit(id, editedCategory);
    }

    public CategoryEntity getById(Long id) {
        return categoryService.getById(id);
    }


    public List<CategoryEntity> getAll() {
        return categoryService.getAll();
    }

    public boolean remove(Long id) {
        return categoryService.remove(id);
    }


}
