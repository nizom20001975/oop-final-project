package org.example.controller;

import org.example.entity.ItemEntity;
import org.example.service.ItemService;

import java.util.List;

public class ItemController {

    private ItemService itemService;

    public ItemController() {
        itemService = new ItemService();
    }

    public List<ItemEntity> add(ItemEntity item) {
       return itemService.add(item);
    }

    public boolean edit(Long id, ItemEntity editedItem) {
       return itemService.edit(id,editedItem);
    }

    public ItemEntity getById(Long id) {
       return itemService.getById(id);
    }


    public List<ItemEntity> getAll() {
       return itemService.getAll();
    }

    public List<ItemEntity> search(String searchingText) {
       return itemService.search(searchingText);
    }

    public boolean remove(Long id) {
       return itemService.remove(id);
    }


}
