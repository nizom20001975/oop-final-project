package org.example.controller;

import org.example.entity.BrandEntity;
import org.example.service.BrandService;

import java.util.List;

public class BrandController {

    private BrandService brandService;

    public BrandController() {
        brandService = new BrandService();
    }

    public List<BrandEntity> add(BrandEntity brand) {
        return brandService.add(brand);
    }

    public boolean edit(Long id, BrandEntity editedBrand) {
        return brandService.edit(id, editedBrand);
    }

    public BrandEntity getById(Long id) {
        return brandService.getById(id);
    }


    public List<BrandEntity> getAll() {
        return brandService.getAll();
    }

    public boolean remove(Long id) {
        return brandService.remove(id);
    }


}
